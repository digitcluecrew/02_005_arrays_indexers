﻿using System;

namespace task_02
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int[] arr = { 3, 2, 5, 7, 12, 3, 1 };

			Console.Write("Array: ");
			foreach (int val in arr)
			{
				Console.Write("{0} ", val);
			}

            Console.WriteLine();

            Console.WriteLine("Max: {0}", Max(arr));
            Console.WriteLine("Min: {0}", Min(arr));
            Console.WriteLine("Sum: {0}", Sum(arr));
            Console.WriteLine("Avg: {0}", Avg(arr));

            Console.Write("Odds: ");
            foreach (int odd in Odd(arr))
            {
                Console.Write("{0} ", odd);
            }

            Console.ReadKey();
        }

        static int Max(int[] arr)
        {
            int max = arr[0];

            foreach (int val in arr)
            {
                if (val > max)
                {
                    max = val;
                }
            }

            return max;
        }

        static int Min(int[] arr)
        {
            int min = arr[0];

            foreach (int val in arr)
            {
                if (val < min)
                {
                    min = val;
                }
            }

            return min;
        }

        static int Sum(int[] arr)
        {
            int sum = 0;

            foreach (int val in arr)
            {
                sum += val;
            }

            return sum;
        }

        static double Avg(int[] arr)
        {
            return (double)Sum(arr) / arr.Length;
        }

        static int[] Odd(int[] arr)
        {
            int[] result = new int[(int)Math.Floor((double)arr.Length / 2)];
            int index = 0;

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = arr[index += 2];
            }

            return result;
        }
    }
}
