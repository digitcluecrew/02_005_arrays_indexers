﻿using System;
namespace task_00
{
    public class Dictionary
    {
        string[] key = new string[5];
        string[] en = new string[5];
        string[] ua = new string[5];

        public Dictionary()
        {
            key[0] = "книга"; en[0] = "book"; ua[0] = "книга";
            key[1] = "ручка"; en[1] = "pen"; ua[1] = "ручка";
            key[2] = "солнце"; en[2] = "sun"; ua[2] = "сонце";
            key[3] = "яблоко"; en[3] = "apple"; ua[3] = "яблуко";
            key[4] = "стол"; en[4] = "table"; ua[4] = "стил";
        }

        public string this[string index]
        {
            get
            {
                for (int i = 0; i < key.Length; i++)
                    if (key[i] == index || en[i] == index || ua[i] == index)
                        return key[i] + " - " + en[i] + " - " + ua[i];

                return string.Format("{0} - нет перевода для этого слова.", index);
            }
        }

        public string this[int index]
        {
            get
            {
                if (index >= 0 && index < key.Length)
                    return key[index] + " - " + en[index] + " - " + ua[index];
                else
                    return "Попытка обращения за пределы массива.";
            }
        }
    }
}
